import logo from './logo.svg';
import './css/App.css';

function App() {
  return (
    <div className="App">
      <div className="titleBox">
        <h1>Mars weather Today.</h1>
      </div>
      <div className="topnav">
        <a className="active" href="#home">Home</a>
        <a href="#news">About Mars</a>
        <a href="#contact">Historical</a>
        <a href="#about">About</a>
      </div>
      <header className="App-header">
      <DateDisplayComponent />
      </header>
    </div>
  );
}

const DateDisplayComponent = (props) => {
  const date = new Date();
  return <p>{date.toDateString()}</p>;
}

// TODO:
// Display date using props
// Display test data using props
// Create a node backend to host and handle data updates.
export default App;
